//
// Created by artam on 26.03.2019.
//

#ifndef PAINT_CLASS_H
#define PAINT_CLASS_H

#include <GLES2/gl2.h>
#include "Matrix.h"
#include "Log.h"
#include <iostream>
#include <thread>
#include <vector>

#include <cstdio>

const GLfloat grid[84]{
        // HORIZONTAL
        -0.8f,  -1.0f,
        0.8f,  -1.0f,
        -0.8f,  -0.8f,
        0.8f,  -0.8f,
        -0.8f,  -0.6f,
        0.8f,  -0.6f,
        -0.8f,  -0.4f,
        0.8f,  -0.4f,
        -0.8f,  -0.2f,
        0.8f,  -0.2f,
        -0.8f,  0.0f,
        0.8f,  0.0f,
        -0.8f,  0.8f,
        0.8f,   0.8f,
        -0.8f,  0.6f,
        0.8f,   0.6f,
        -0.8f,  0.4f,
        0.8f,   0.4f,
        -0.8f,  0.2f,
        0.8f,   0.2f,
        -0.8f,  1.0f,
        0.8f,   1.0f,
        // VERTICAL
        -0.8f, -1.0f,
        -0.8f, 1.0f,
        -0.6f, -1.0f,
        -0.6f, 1.0f,
        -0.4f, -1.0f,
        -0.4f, 1.0f,
        -0.2f, -1.0f,
        -0.2f, 1.0f,
        0.0f, -1.0f,
        0.0f, 1.0f,
        0.8f, -1.0f,
        0.8f, 1.0f,
        0.6f, -1.0f,
        0.6f, 1.0f,
        0.4f, -1.0f,
        0.4f,  1.0f,
        0.2f, -1.0f,
        0.2f, 1.0f
};



class Paint {

public:

    Paint();

    ~Paint();

    void create();

    void change(int width, int height);
    void draw();

    void addPoint(  int  ,  int );
    void clear();

private:
    Matrix *mViewMatrix;
    Matrix *mModelMatrix;
    Matrix *mProjectionMatrix;
    Matrix *mMVPMatrix;
    GLuint mProgram;
    GLuint mMVPMatrixHandle;
    GLuint mPositionHandle;
    GLuint mColorHandle;
    void drawPoint();
    void drawGrid();
    std::vector<float>* data = new std::vector<float>();
};


#endif //PAINT_CLASS_H
