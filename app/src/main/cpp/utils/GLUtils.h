//
// Created by artam on 26.03.2019.
//

#ifndef TETRIS_GLUTILS_H
#define TETRIS_GLUTILS_H

#include <jni.h>

#include <EGL/egl.h>
#include <GLES2/gl2.h>
#include <GLES2/gl2ext.h>
#include "Log.h"

class GLUtils {
public:
    static GLuint createProgram(const char **vertexSource, const char **fragmentSource);

};



#endif //TETRIS_GLUTILS_H
