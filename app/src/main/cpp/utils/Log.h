//
// Created by artam on 29.03.2019.
//

#ifndef TETRIS_LOG_H

#define TETRIS_LOG_H

#include <android/log.h>


#define LOG_TAG "Tetris"
#define LOGI(fmt, args...) __android_log_print(ANDROID_LOG_INFO, LOG_TAG, fmt, ##args)
#define LOGD(fmt, args...) __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, fmt, ##args)
#define LOGE(fmt, args...) __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, fmt, ##args)


#endif //TETRIS_LOG_H
