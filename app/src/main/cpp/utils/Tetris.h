//
// Created by artam on 26.03.2019.
//

#ifndef TETRIS_CLASS_H
#define TETRIS_CLASS_H

#include <GLES2/gl2.h>
#include "Matrix.h"
#include "Paint.h"
#include <iostream>
#include <thread>
#include <vector>
#include <jni.h>

using namespace std;

#include <cstdio>


const uint8_t FIELD_WIDTH = 9;

const uint8_t FIELD_HEIGHT = 14;

static const uint8_t figures[5][16] = {
        {
                0,1,0,0,  //line
                0,1,0,0,
                0,1,0,0,
                0,1,0,0,
        },
        {
                0,0,1,0, // T
                0,1,1,0,
                0,0,1,0,
                0,0,0,0,
        },
        {
                0,0,0,0, // box
                0,1,1,0,
                0,1,1,0,
                0,0,0,0,
        },
        {
                0,0,1,0, // Z
                0,1,1,0,
                0,1,0,0,
                0,0,0,0,
        },
        {
                0,0,1,0, // L
                0,0,1,0,
                0,1,1,0,
                0,0,0,0,
        },
};

#define MOVE_DOWN   0
#define MOVE_LEFT  (-1)
#define MOVE_RIGHT 1

#define GAME_OVER 0
#define SCORE 1

class Tetris {
public:

    Tetris();

    ~Tetris();
    Paint paint;
    int score = 0;
    bool gameOver = false;
    void move(int dir);
    void draw();
    void rotate();
    void clear();
private:
    uint8_t pField[FIELD_HEIGHT][FIELD_WIDTH];
    // Game Logic
    uint8_t nCurrentPiece = 0;
    uint8_t angleNumber = 0;
    int8_t nCurrentX = FIELD_WIDTH / 2 -2;
    int8_t nCurrentY = 0;
    bool needUpdate = true;
    void finishFigure();
    void checkLines();
    void newFigure();
    bool isFit(int figure, int posX, int posY);
    int rotate(int px, int py );
};

#endif //TETRIS_CLASS_H
