//
// Created by artam on 26.03.2019.
//

#include "Paint.h"


#include "GLUtils.h"
#include <iostream>
#include<cstdlib>


  void printGLString(const char *name, GLenum s) {
    const char *v = (const char *) glGetString(s);
    LOGI("GL %s = %s \n", name, v);
}

  void checkGlError(const char *op) {
    for (GLint error = glGetError(); error; error = glGetError()) {
        LOGI("after %s() glError (0x%x)\n", op, error);
    }
}

static const char *VERTEX_SHADER =
        "uniform mat4 u_MVPMatrix;        \n"
        "attribute vec4 a_Position;     \n"
        "void main()                    \n"
        "{                              \n"
        "   gl_Position = u_MVPMatrix * a_Position; \n"
         "}                              \n";

static const char *FRAGMENT_SHADER = "precision mediump float;         \n"
                              "uniform vec4 u_Color;          \n"
                              "void main()                    \n"
                              "{                              \n"
                              "   gl_FragColor = u_Color;      \n"
                              "}                              \n";


Paint::Paint() {
    mModelMatrix = nullptr;
    mMVPMatrix = nullptr;
    mProjectionMatrix = nullptr;
    mViewMatrix = nullptr;
}

Paint::~Paint() {
    delete mModelMatrix;
    mModelMatrix = nullptr;
    delete mMVPMatrix;
    mMVPMatrix = nullptr;
    delete mProjectionMatrix;
    mProjectionMatrix = nullptr;
    delete mViewMatrix;
    mViewMatrix = nullptr;
}

void Paint::create() {

    printGLString("Version", GL_VERSION);
    printGLString("Vendor", GL_VENDOR);
    printGLString("Renderer", GL_RENDERER);
    printGLString("Extensions", GL_EXTENSIONS);

    mProgram = GLUtils::createProgram(&VERTEX_SHADER, &FRAGMENT_SHADER);
    if (!mProgram) {
        LOGD("Could not create program");
        return;
    }

    mModelMatrix = new Matrix();
    mMVPMatrix = new Matrix();

    const float eyeX =  0.0f;
    const  float eyeY = 0.0f;
    const  float eyeZ = 1.0f;

    const  float centerX = 0.0f;
    const  float centerY = 0.0f;
    const  float centerZ = 0.0f;

    const  float upX = 0.0f;
    const  float upY = 1.0f;
    const  float upZ = 0.0f;

    mViewMatrix = Matrix::newLookAt(eyeX, eyeY, eyeZ, centerX, centerY, centerZ, upX, upY, upZ);
}

void Paint::change(int width, int height) {
    LOGD("change ");
    create();

    glViewport(0, 0, width, height);

    float ratio = (float) width / height;
    float left = -ratio;
    float right = ratio;
    float bottom = -1.0f;
    float top = 1.0f;
    float near = 0.8f;
    float far = 3.0f;

    mProjectionMatrix = Matrix::newFrustum(left, right, bottom, top, near, far);
}

void Paint::draw() {

    glClearColor(0.5F, 0.5F, 0.5F, 0.5F);
    glClear(GL_COLOR_BUFFER_BIT);
    glLineWidth(2);
    checkGlError("glClear");
    glUseProgram(mProgram);
    mMVPMatrixHandle = (GLuint) glGetUniformLocation(mProgram, "u_MVPMatrix");
    mPositionHandle = (GLuint) glGetAttribLocation(mProgram, "a_Position");
    mColorHandle = (GLuint) glGetUniformLocation(mProgram, "u_Color");

    glUniform4f(mColorHandle, 1.0f,  1.0f, 1.0f, 1.0f);
    mModelMatrix->identity();

    drawGrid();
    drawPoint();
}

void Paint::drawPoint() {

    glVertexAttribPointer(
            (GLuint) mPositionHandle,
            2,
            GL_FLOAT,
            GL_FALSE,
            4 * 2,
            data->data()
    );
    glEnableVertexAttribArray((GLuint) mPositionHandle);

    mMVPMatrix->multiply(*mViewMatrix, *mModelMatrix);

    mMVPMatrix->multiply(*mProjectionMatrix, *mMVPMatrix);

    glUniformMatrix4fv(mMVPMatrixHandle, 1, GL_FALSE, mMVPMatrix->mData);

    const size_t size = data->size()/2;
    for (int i = 0; i < size;  i += 4) {
        glDrawArrays(GL_TRIANGLE_STRIP , i, 4);
    }

    checkGlError("drawPoint " );
}

void Paint::drawGrid() {

    glVertexAttribPointer(
            (GLuint) mPositionHandle,
            2,
            GL_FLOAT,
            GL_FALSE,
            4 * 2,
            grid
    );
    glEnableVertexAttribArray((GLuint) mPositionHandle);

    mMVPMatrix->multiply(*mViewMatrix, *mModelMatrix);

    mMVPMatrix->multiply(*mProjectionMatrix, *mMVPMatrix);

    glUniformMatrix4fv(mMVPMatrixHandle, 1, GL_FALSE, mMVPMatrix->mData);

    for (int i = 0; i < 21;  i++) {
        glDrawArrays(GL_LINES, 2*i, 2);
    }

    checkGlError("drawGrid");
}
void Paint::addPoint(  int x,   int y) {
     if(y < 4) return;
     // LOGI("px %d py %d ", x, y);
     float tx = (float)x;
     float ty = ((float)y)*(-1.0f);
 //   LOGI("px %f py %f ",tx, ty);

     data->push_back( tx*0.2f  - 1.0f) ;
     data->push_back( ty*0.2f  + 1.8f) ;

     data->push_back( tx*0.2f  - 1.0f) ;
     data->push_back( ty*0.2f  + 1.6f) ;

     data->push_back( tx*0.2f  - 0.8f) ;
     data->push_back( ty*0.2f  + 1.8f) ;

     data->push_back( tx*0.2f  - 0.8f) ;
     data->push_back( ty*0.2f  + 1.6f) ;

   //  LOGI("size %d ", (int)data.size());
}
void Paint::clear() {
    data->clear();
}

