//
// Created by artam on 26.03.2019.
//

#include "Tetris.h"


#include "GLUtils.h"
#include <iostream>
#include<cstdlib>
#include<ctime>
#include "Paint.h"



Tetris::Tetris() {
    clear();
}

Tetris::~Tetris() {
}


bool Tetris::isFit(int figure, int posX, int posY)
{
    for (uint8_t px = 0; px < 4; px++)
        for (uint8_t py = 0; py < 4; py++)
        {
            // Get index into piece
            int pi = rotate(px, py);

            if (figures[figure][pi] == 1 ) {
                if ((posX + px) <= 0  ||
                     (posX + px) >= FIELD_WIDTH ||
                      (posY + py) >= FIELD_HEIGHT ||
                        pField[posY + py][posX + px] == 1) {
                            return false;
                }
            }
        }
    return true;
}


void Tetris::draw()
{

    if (!gameOver && needUpdate)
    {
        paint.clear();

        // Check Lines Fill
        checkLines();

        // Draw Field
        for (uint8_t x = 0; x < FIELD_WIDTH ; x++) {
            for (uint8_t y = 0; y < FIELD_HEIGHT; y++) {
               if (pField[y][x] == 1)  {
                   paint.addPoint(x, y);
               }
            }
        }

        // Draw Current Piece
        for (uint8_t px = 0; px < 4; px++) {
            for (uint8_t py = 0; py < 4; py++)
                if (figures[nCurrentPiece][rotate(px, py)] != 0) {
                    //int index =  (nCurrentY + py + 2)*FIELD_WIDTH + nCurrentX + px + 2 ;
                     paint.addPoint(nCurrentX + px, nCurrentY + py);
                }
        }

        needUpdate = false;
    }

    paint.draw();
}


void Tetris::move(int dir) {
    if(gameOver)return;
    // Handle   movement
    switch (dir){
        case MOVE_RIGHT:{
            if(isFit(nCurrentPiece, nCurrentX + 1, nCurrentY))  nCurrentX++;
        }break;
        case MOVE_LEFT:{
            if(isFit(nCurrentPiece, nCurrentX - 1, nCurrentY))  nCurrentX--;
        }break;
        case MOVE_DOWN:{
            if(isFit(nCurrentPiece, nCurrentX  , nCurrentY + 1) )
                nCurrentY++;
            else
                finishFigure();
        }break;
    }
    needUpdate = true;
}


int Tetris::rotate(int px, int py)
{
    int pi = 0;
    switch (angleNumber)
    {
        case 0: // 0 degrees
            pi = py * 4 + px;
            break;
        case 1: // 90 degrees
            pi = 12 + py - (px * 4);
            break;
        case 2: // 180 degrees
            pi = 15 - (py * 4) - px;
            break;
        case 3: // 270 degrees
            pi = 3 - py + (px * 4);
            break;
    }
    return pi;
}

void Tetris::rotate() {
    if(gameOver)return;
    angleNumber++;
    if(isFit(nCurrentPiece, nCurrentX, nCurrentY) ){
        angleNumber %= 4;
    } else
        angleNumber--;

    needUpdate = true;
}

void  Tetris::finishFigure(){

    for (int px = 0; px < 4; px++)
        for (int py = 0; py < 4; py++)
            if (figures[nCurrentPiece][rotate(px, py)] != 0)
                pField[nCurrentY + py][nCurrentX + px] = 1;

    // Pick New Piece
    newFigure();
    score += 25;
    // If piece does not fit straight away, game over!
    gameOver = !isFit(nCurrentPiece, nCurrentX, nCurrentY + 2);
}


void Tetris::checkLines() {
    // Check for lines
    //LOGI(" Check for lines %d ", nCurrentY);

    for ( int8_t py = FIELD_HEIGHT - 1; py > 0  ; --py)  {

        bool bLine = true;

        for (uint8_t px = 1; px < FIELD_WIDTH; px++){
            uint8_t index = pField[ py][px];
            if(index != 1){
                bLine = false;
                break;
            }
          //  LOGI("   bLine %d ", index);
        }
        if (bLine)
        {
           // LOGI(" finish bLine ");
            for (int sx = 1; sx < FIELD_WIDTH ; ++sx)
            {
                for (int8_t sy = py; sy > 0; sy--)
                    pField[sy][sx] = pField[ sy - 1][ sx];
            }
            score += 100;
        }
    }
}
void Tetris::newFigure() {

    // Pick New Piece
    nCurrentX = FIELD_WIDTH / 2 - 2;
    nCurrentY = 0;
    angleNumber = 0;
    srand((unsigned int)time(nullptr));
    nCurrentPiece =  (uint8_t)(rand() % 5 );
}

void Tetris::clear() {
    LOGD("CLEAR ");
    gameOver = false;

    for (uint8_t x = 0; x < FIELD_WIDTH ; x++) {
        for (uint8_t y = 0; y < FIELD_HEIGHT; y++) {
             pField[y][x]  = 0;
        }
    }
    // Pick New Piece
    newFigure();
    score = 0;
    paint.clear();
    needUpdate = true;
}
