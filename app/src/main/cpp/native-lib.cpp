
#include <jni.h>
#include "Tetris.h"

/// =======================================================


Tetris* tetris;

extern "C" {
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_init(JNIEnv * env, jobject obj);
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_change(JNIEnv * env, jobject obj,  jint width, jint height);
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_draw(JNIEnv * env, jobject obj);
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_move(JNIEnv * env, jobject obj, jint direction);
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_rotate(JNIEnv * env, jobject obj);
JNIEXPORT jint JNICALL Java_com_online_job_tetris_JNITetris_score(JNIEnv * env, jobject obj);
JNIEXPORT jint JNICALL Java_com_online_job_tetris_JNITetris_status(JNIEnv * env, jobject obj);
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_clear(JNIEnv * env, jobject obj);
};

JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_init(JNIEnv * env, jobject obj)
{
    if(tetris == nullptr){
        tetris  = new Tetris();
    }
}

JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_change(JNIEnv * env, jobject obj,  jint width, jint height)
{
    if(tetris != nullptr)
        tetris->paint.change(width, height);
}
JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_draw(JNIEnv * env, jobject obj)
{

    if(tetris != nullptr)
        tetris->draw();
}

JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_move(JNIEnv * env, jobject obj, jint direct)
{
    if(tetris != nullptr)
        tetris->move(direct);
}

JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_rotate(JNIEnv * env, jobject obj)
{
    if(tetris != nullptr)
        tetris->rotate();
}


JNIEXPORT jint JNICALL Java_com_online_job_tetris_JNITetris_score(JNIEnv * env, jobject obj)
{
    if(tetris != nullptr)
        return  tetris->score;
    else
        return 0;
}

JNIEXPORT jint JNICALL Java_com_online_job_tetris_JNITetris_status(JNIEnv * env, jobject obj)
{
    bool status =   !tetris->gameOver;
    if(tetris != nullptr)
        return status;
    else
        return 0;
}


JNIEXPORT void JNICALL Java_com_online_job_tetris_JNITetris_clear(JNIEnv * env, jobject obj)
{
    LOGD("CLEAR ");
    if(tetris != nullptr){
        tetris->clear();
    }
}