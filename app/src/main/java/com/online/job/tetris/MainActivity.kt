package com.online.job.tetris

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.opengl.GLSurfaceView
import android.widget.Toast
import android.app.ActivityManager
import android.app.AlertDialog
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.view.MotionEvent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    private lateinit var glSurfaceView: GLSurfaceView

    private val levelsTitle = arrayOf("Level 1","Level 2","Level 3","Crazy Mode" )
    private val levels = arrayOf(1,2,3,4,20)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_main)

        if (!supportES2()) {
            Toast.makeText(this, "OpenGl ES 2.0 is not supported", Toast.LENGTH_LONG).show()
            finish()
            return
        }
        glSurfaceView = surfaceView

        left.setOnTouchListener(MoveListener(-1))

        right.setOnTouchListener(MoveListener( 1))

        down.setOnTouchListener(MoveListener( 0))

        rotate.setOnClickListener {
            JNITetris.rotate()
        }

        startPause.setOnClickListener {
            gameStatus =  if(gameStatus == FINISH){
                JNITetris.clear()
                startPause.text = getString(R.string.pause)
                    PLAY
            }else{
                if (gameStatus == PLAY){
                    startPause.text = getString(R.string.start)
                    PAUSE
                } else {
                    startPause.text = getString(R.string.pause)
                    PLAY
                }
            }
        }
        exit.setOnClickListener {
            gameStatus = -1
            JNITetris.clear()
            finish()
        }
        if (gameStatus == PLAY)
            startPause.text = getString(R.string.pause)
         else
            startPause.text = getString(R.string.start)

        processGame()
    }

   private  class  MoveListener(val dir : Int) : View.OnTouchListener{
        var hasTouch = false

        override fun onTouch(v: View , event: MotionEvent ): Boolean {
            when(event.action){
                MotionEvent.ACTION_DOWN ->{
                    hasTouch = true
                    v.performClick()
                    Thread{
                        while (hasTouch){
                            JNITetris.move(dir)
                            Thread.sleep(100)
                        }
                    }.start()
                }
                MotionEvent.ACTION_UP ->{
                    hasTouch = false
                }
            }
            return true
        }
    }
    private fun processGame(){
        Handler(Looper.getMainLooper()).postDelayed({
            if(gameStatus == PLAY)
              moveDown()
            processGame()
        },1000L/level)
    }
    private fun pauseGame(){
        if(gameStatus == PLAY){
            gameStatus = PAUSE
            startPause.text = getString(R.string.start)
        }

    }
    private fun resumeGame(){
        if(gameStatus == PAUSE){
            gameStatus = PLAY
            startPause.text = getString(R.string.pause)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        pauseGame()
    }
    override fun onPause() {
        super.onPause()
        pauseGame()
        glSurfaceView.onPause()
    }

    override fun onResume() {
        super.onResume()
        resumeGame()
        glSurfaceView.onResume()
    }


    private fun supportES2(): Boolean {
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val configurationInfo = activityManager.deviceConfigurationInfo
        return configurationInfo.reqGlEsVersion >= 0x20000
    }

    private fun moveDown(){
        if( gameStatus != PLAY) return

        JNITetris.move( 0)
        val sc = JNITetris.score()
        if(sc > score){
                score = sc
                scoreTxt.text = getString(R.string.score, score)
        }

        if( JNITetris.status() == FINISH && gameStatus != FINISH){
            gameStatus = FINISH
            startPause.text = getString(R.string.restart)
            scoreTxt.text = getString(R.string.game_over )

            AlertDialog.Builder(this)
                .setTitle(R.string.game_over)
                .setMessage(getString(R.string.score, score))
                .setItems(levelsTitle){_,number : Int->
                    level = levels[number]
                }
                .setPositiveButton(  R.string.change_level ) {_,_->
                     AlertDialog.Builder(this)
                        .setTitle(R.string.change_level)
                        .setItems(levelsTitle){_,number : Int->
                            level = levels[number]
                        }
                        .show()
                }
                .setNegativeButton(R.string.cancel, null)
                .show()
        }
    }

    companion object {
        private const val PLAY = 1
        private const val PAUSE = 2
        private const val FINISH = 0
        private var level = 1
        private var gameStatus = -1
        private var score = 0
    }
}
