package com.online.job.tetris.view

import android.content.Context
import android.opengl.GLSurfaceView
import android.util.AttributeSet
import com.online.job.tetris.JNITetris
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

class MyGLSurfaceView(context: Context, attributes: AttributeSet?) : GLSurfaceView(context, attributes ){



    constructor(context: Context  )  : this(context, null)

    init {
        setEGLContextClientVersion(2)
        setRenderer(Renderer())
    }

    private class Renderer : GLSurfaceView.Renderer {
        override fun onDrawFrame(gl: GL10) {
            JNITetris.draw()
        }

        override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
            JNITetris.change(width, height)
        }

        override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
            JNITetris.init()
        }
    }
}
