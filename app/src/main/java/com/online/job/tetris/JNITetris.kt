package com.online.job.tetris

// Wrapper for native library

object JNITetris {

    init {
        System.loadLibrary("native-lib")
    }

    external fun init()
    external fun change(width: Int, height: Int)
    external fun draw()
    external fun move(direction: Int)
    external fun rotate()
    external fun clear()
    external fun status(): Int
    external fun score(): Int
}